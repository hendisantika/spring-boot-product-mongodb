package com.hendisantika.springbootproductmongodb.service;

import com.hendisantika.springbootproductmongodb.domain.Product;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-product-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-22
 * Time: 09:07
 */

public interface ProductService {
    Optional<Product> readById(String id);

    List<Product> readAll();

    Product update(String id, Product product);

    Product create(Product product);

    void delete(String id);
}