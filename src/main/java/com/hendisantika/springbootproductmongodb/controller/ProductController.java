package com.hendisantika.springbootproductmongodb.controller;

import com.hendisantika.springbootproductmongodb.domain.Product;
import com.hendisantika.springbootproductmongodb.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-product-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-22
 * Time: 09:07
 */
@RestController
@RequestMapping("/products")
@Tag(name = "Products", description = "Endpoint for managing Products")
public class ProductController {

    @Autowired
    private ProductService service;

    @PostMapping
//    @ApiOperation(value = "Add New Product", produces = "application/json", response = Product.class)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Internal Error")})
    @Operation(
            summary = "Add New Product Data",
            description = "Add New Product Data.",
            tags = {"Products"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public Product create(@RequestBody Product product) {
        return service.create(product);
    }

    @GetMapping
//    @ApiOperation(value = "Get All Products", produces = "application/json", response = Product.class)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Internal Error")})
    @Operation(
            summary = "Get All Tutorials Data",
            description = "Get All Tutorials Data.",
            tags = {"Products"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    public List<Product> readAll() {
        return service.readAll();
    }

    @GetMapping(value = "/{id}")
//    @ApiOperation(value = "Find Product By Id", produces = "application/json", response = Product.class)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Internal Error")})
    @Operation(
            summary = "Get Product Data By ID",
            description = "Get Product Data By ID.",
            tags = {"Products"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    @Parameter(in = ParameterIn.PATH, name = "productId", schema = @Schema(type = "string"))
    public Optional<Product> readById(@PathVariable("id") String id) {
        return service.readById(id);
    }

    @PatchMapping(value = "/{id}")
//    @ApiOperation(value = "Update Product by Id", produces = "application/json", response = Product.class)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Internal Error")})
    @Operation(
            summary = "Update Product Data By ID",
            description = "Update Product Data By ID.",
            tags = {"Products"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    @Parameter(in = ParameterIn.PATH, name = "productId", schema = @Schema(type = "string"))
    public Product update(@PathVariable("id") String id, @RequestBody Product product) {
        return service.update(id, product);
    }

    @DeleteMapping(value = "/{id}")
//    @ApiOperation(value = "Delete Product By Id", produces = "application/json", response = Product.class)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Internal Error")})
    @Operation(
            summary = "Delete Product Data By ID",
            description = "Delete Product Data By ID.",
            tags = {"Products"})
    @ApiResponses(value = {
            @io.swagger.v3.oas.annotations.responses.ApiResponse(
                    description = "Success",
                    responseCode = "200",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            Product.class))
            ),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Not found", responseCode = "404",
                    content = @Content),
            @io.swagger.v3.oas.annotations.responses.ApiResponse(description = "Internal error", responseCode = "500"
                    , content = @Content)
    }
    )
    @Parameter(in = ParameterIn.PATH, name = "productId", schema = @Schema(type = "string"))
    public void delete(@PathVariable("id") String id) {
        service.delete(id);
    }
}
