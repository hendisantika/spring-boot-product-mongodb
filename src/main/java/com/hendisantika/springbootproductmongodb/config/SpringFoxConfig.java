package com.hendisantika.springbootproductmongodb.config;

import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-product-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-22
 * Time: 09:08
 */
@Configuration
public class SpringFoxConfig {

    // Inject API info property values
    @Value("${api.title}")
    private String title;

    @Value("${api.description}")
    private String description;


    @Value("${api.version}")
    private String version;


    @Value("${api.termOfServiceUrl}")
    private String termOfServiceUrl;


    @Value("${api.contactName}")
    private String contactName;

    @Value("${api.contactUrl}")
    private String contactUrl;

    @Value("${api.contactEmail}")
    private String contactEmail;

//    @Bean
//    public Docket apiDocket() {
//        //@formatter:off
//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.hendisantika.springbootproductmongodb.controller"))
//                .paths(PathSelectors.any())
//                .build()
//                .apiInfo(getApiInfo());
//        //@formatter:on
//    }
//
//    private ApiInfo getApiInfo() {
//        //@formatter:off
//        return new ApiInfo(
//                title,
//                description,
//                version,
//                termOfServiceUrl,
//                new Contact(contactName, contactUrl, contactEmail),
//                "LICENSE",
//                "LICENSE URL",
//                Collections.emptyList()
//        );
//        //@formatter:on
//    }

    @Bean
    public OpenAPI notifOpenAPI() {
        OpenAPI openAPI = new OpenAPI();
        openAPI.info(new io.swagger.v3.oas.models.info.Info()
                .title(title)
                .description(description)
                .version(version)
                .contact(new io.swagger.v3.oas.models.info.Contact()
                        .name(contactName)
                        .url(contactUrl)
                        .email(contactEmail))
                .termsOfService(termOfServiceUrl)
                .license(new io.swagger.v3.oas.models.info.License().name("License").url("https://s.id/hendisantika")));
        return openAPI;
    }
}
