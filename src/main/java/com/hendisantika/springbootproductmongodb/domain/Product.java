package com.hendisantika.springbootproductmongodb.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-product-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-22
 * Time: 09:04
 */

@Document(collection = "product")
@Schema(description = "Product model")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @Schema(description = "id in mongodb store", accessMode = Schema.AccessMode.READ_ONLY)
    private String id;

    @Schema(description = "stock keeping unit")
    private Integer sku;

    @Schema(description = "product description")
    private String description;

    @Schema(description = "product base price")
    private BigDecimal price;
}
