# Spring Boot Product MongoDB CRUD

#### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-product-mongodb.git`
2. Navigate to the folder: `cd spring-boot-product-mongodb`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080/swagger-ui

Add New Product

```shell
curl -X 'POST' \
  'http://localhost:8080/products' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '  {
    "sku": 3,
    "description": "buku Sinar Dunia Akhirat",
    "price": 2500
  }
'
```

Body Response:

```shell
{
  "id": "668381a9eeee206496c2f22c",
  "sku": 3,
  "description": "buku Sinar Dunia Akhirat",
  "price": 2500
}
```

### Image Screenshot

Swagger UI

![Swagger UI](img/Swagger-UI.png "Swagger UI")

